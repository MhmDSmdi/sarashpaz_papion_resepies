package com.example.asus.sarashpazpapillon.Fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.asus.sarashpazpapillon.Adapter.ResepiCardAdapter;
import com.example.asus.sarashpazpapillon.HelperClasses.HelperClass;
import com.example.asus.sarashpazpapillon.HelperClasses.SessionManager;
import com.example.asus.sarashpazpapillon.Models.ResepiCard;
import com.example.asus.sarashpazpapillon.Models.ResepiList;
import com.example.asus.sarashpazpapillon.R;
import com.example.asus.sarashpazpapillon.Retrofit.ApiClient;
import com.example.asus.sarashpazpapillon.Retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asus on 9/2/2017.
 */

public class LastResepi extends Fragment {

    private RecyclerView recyclerView;
    private ResepiCardAdapter adapter = null;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<ResepiCard> cardsList;
    private List<ResepiCard> resepiCardListTemp;
    private String url;
    private int indexOffset = 0;
    private FloatingActionButton fab;
    private View view;
    private SessionManager sessionManager;
    private TextView txtLoading;
    private TextView loadFailed;
    private ImageView reLoad;
    private ProgressBar progressBar;
    private boolean isFailed = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");
        cardsList = new ArrayList<>();
        resepiCardListTemp = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFailed) {
            loadFailed.setVisibility(View.VISIBLE);
            reLoad.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.last_resepi, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        sessionManager = new SessionManager(view.getContext());
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        txtLoading = (TextView) getActivity().findViewById(R.id.txtLoading);
        loadFailed = (TextView) view.findViewById(R.id.txtFailorLoad);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        reLoad = (ImageView) view.findViewById(R.id.reLoadPage);
        reLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchDataByOffset(indexOffset);
            }
        });
        fetchDataByOffset(indexOffset);
        setupScrollListener();
        setupRecycler();
        return view;
    }


    private void setupRecycler() {
        adapter = new ResepiCardAdapter(view.getContext(), cardsList, sessionManager);
        mLayoutManager = new GridLayoutManager(view.getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void setupScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy>0) {
                    final int visibleThreshold = 2;
                    GridLayoutManager layoutManager = (GridLayoutManager)recyclerView.getLayoutManager();
                    int lastItem  = layoutManager.findLastCompletelyVisibleItemPosition();
                    int currentTotalCount = layoutManager.getItemCount();

                    if(currentTotalCount <= lastItem + visibleThreshold){
                        txtLoading.setVisibility(View.VISIBLE);
                        fetchDataByOffset(indexOffset);
                    }

                    fab.hide();
                }

                if (dy<0) {
                    fab.show();
                }
            }
        });
    }

    private void fetchDataByOffset(int offset) {
        if (cardsList.size() == 0) {
            progressBar.setVisibility(View.VISIBLE);
            loadFailed.setVisibility(View.INVISIBLE);
            reLoad.setVisibility(View.INVISIBLE);
        }

        ApiInterface apiInterface = ApiClient.getApiService();
        Call<ResepiList> call = apiInterface.getResepiList(url + offset);
        call.enqueue(new Callback<ResepiList>() {
            @Override
            public void onResponse(Call<ResepiList> call, Response<ResepiList> response) {
                resepiCardListTemp = getRTLList(response.body().getResepiCards());
                for (ResepiCard myCard : resepiCardListTemp) {
                    myCard.setNum_like(HelperClass.number2farsi(myCard.getNum_like()));

                    if (!sessionManager.myPreferences.contains(myCard.getHid()))
                        sessionManager.setLikeView(myCard.getHid(), myCard.isLiked());
                    else
                        myCard.setLiked(sessionManager.myPreferences.getBoolean(myCard.getHid(), true));
                }
                cardsList.addAll(resepiCardListTemp);
                progressBar.setVisibility(View.INVISIBLE);
                adapter.notifyDataSetChanged();
                indexOffset += 10;
                txtLoading.setVisibility(View.INVISIBLE);
                loadFailed.setVisibility(View.INVISIBLE);
                reLoad.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                isFailed = false;
            }

            @Override
            public void onFailure(Call<ResepiList> call, Throwable t) {
                loadFailed.setVisibility(View.VISIBLE);
                reLoad.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                isFailed = true;
            }
        });
    }

    private List<ResepiCard> getRTLList(List<ResepiCard> list){
        List<ResepiCard> rtlList = new ArrayList<>();
        ResepiCard resepiTemp = null;
        for (int i = 0 ; i < list.size() ; i++) {
            if (i%2 == 0)
                resepiTemp = list.get(i);
            else {
                rtlList.add(list.get(i));
                rtlList.add(resepiTemp);
            }
        }

        return rtlList;
    }
}
