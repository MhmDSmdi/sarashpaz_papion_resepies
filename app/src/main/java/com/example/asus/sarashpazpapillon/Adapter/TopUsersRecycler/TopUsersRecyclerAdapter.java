package com.example.asus.sarashpazpapillon.Adapter.TopUsersRecycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.asus.sarashpazpapillon.Models.User;
import com.example.asus.sarashpazpapillon.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Asus on 9/5/2017.
 */

public class TopUsersRecyclerAdapter extends RecyclerView.Adapter<TopUsersRecyclerAdapter.TopUserHolder> {

    private Context mContext;
    private List<User> userList;

    public TopUsersRecyclerAdapter(Context mContext, List<User> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }

    @Override
    public TopUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.top_user_card, parent, false);
        return new TopUsersRecyclerAdapter.TopUserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopUserHolder holder, int position) {

        User user = userList.get(position);
        holder.userName.setText(user.getName());
        holder.score.setText(user.getScore() + " امتیاز ");

        Glide.with(mContext)
                .load(user.getProfileImage())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.guestuser)
                .into(holder.profileImage);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class TopUserHolder extends RecyclerView.ViewHolder {

        TextView userName, score;
        CircleImageView profileImage;
        Button btnFollow;

        public TopUserHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.userName);
            score = (TextView) itemView.findViewById(R.id.userScore);
            profileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);
            btnFollow = (Button) itemView.findViewById(R.id.btnFollow);
        }
    }
}
