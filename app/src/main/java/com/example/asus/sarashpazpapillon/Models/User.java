package com.example.asus.sarashpazpapillon.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Asus on 9/5/2017.
 */

public class User {

    @SerializedName("name")
    private String name;

    @SerializedName("points")
    private String score;

    @SerializedName("pic_url")
    private String profileImage;

    @SerializedName("hid")
    private String hid;


    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProfileImage(String profile) {
        this.profileImage = profile;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }
}
