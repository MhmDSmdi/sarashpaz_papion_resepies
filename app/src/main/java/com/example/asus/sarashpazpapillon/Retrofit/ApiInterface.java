package com.example.asus.sarashpazpapillon.Retrofit;

import com.example.asus.sarashpazpapillon.Models.ResepiList;
import com.example.asus.sarashpazpapillon.Models.TabList;
import com.example.asus.sarashpazpapillon.Models.UserList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Asus on 9/2/2017.
 */

public interface ApiInterface {
   // @GET("ai300/recipe/recent/0")
    @GET
    Call<ResepiList> getResepiList(@Url String url);

    @GET
    Call<TabList> getTabList(@Url String url);

    @GET
    Call<UserList> getUserList(@Url String url);

}
