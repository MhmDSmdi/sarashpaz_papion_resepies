package com.example.asus.sarashpazpapillon;


import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/sl.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}

