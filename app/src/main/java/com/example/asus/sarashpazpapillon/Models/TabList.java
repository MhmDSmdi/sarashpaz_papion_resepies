package com.example.asus.sarashpazpapillon.Models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 9/3/2017.
 */

public class TabList {

    @SerializedName("tabs")
    private List<Tab> tabs;

    public TabList() {
        tabs = new ArrayList<>();
    }

    public void fetchTabs() throws JSONException {


    }

    public List<Tab> getTabs() {
        return tabs;
    }

}
