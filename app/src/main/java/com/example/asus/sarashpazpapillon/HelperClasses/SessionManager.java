package com.example.asus.sarashpazpapillon.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

/**
 * Created by Asus on 9/12/2017.
 */

public class SessionManager {

    public SharedPreferences myPreferences;
    public SharedPreferences.Editor editor;
    public Context context;
    public List<String> sharedList;
    public static final String IS_FIRST_START = "isFirst";

    public SessionManager(Context context) {
        this.context = context;
        myPreferences = context.getSharedPreferences("myPref", 0);
        editor = myPreferences.edit();
        editor.putBoolean(IS_FIRST_START, true);
    }

    public void setLikeView(String viewKey, boolean value){
        editor.remove(viewKey);
        editor.putBoolean(viewKey, value);
        editor.commit();
    }
}
