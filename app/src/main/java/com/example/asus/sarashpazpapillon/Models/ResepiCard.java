package com.example.asus.sarashpazpapillon.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Asus on 8/28/2017.
 */

public class ResepiCard {

    @SerializedName("name")
    private String title;

    @SerializedName("img_url")
    private String foodImage;

    @SerializedName("like_num")
    private String num_like;

    @SerializedName("user")
    private User thisUser;

    @SerializedName("hid")
    private String hid;

    private transient boolean isLiked = false;

    public ResepiCard() {
        thisUser = new User();
    }

    public ResepiCard(String title, String num_like) {
        this.title = title;
        this.num_like = num_like;
    }

    public String getTitle() {
        return title;
    }


    public String getNum_like() {
        return num_like;
    }

    public String getFoodImage() {
        return foodImage;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNum_like(String num_like) {
        this.num_like = num_like;
    }

    public void setFoodImage(String foodImage) {
        this.foodImage = foodImage;
    }

    public User getThisUser() {
        return thisUser;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }
}
