package com.example.asus.sarashpazpapillon.Models;

import android.support.v4.app.Fragment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Asus on 9/3/2017.
 */

public class Tab {

    @SerializedName("name")
    private String toolBarName;

    @SerializedName("url")
    private String url;

    @SerializedName("tab_type")
    private int tabType;

    private transient Fragment parentFragment;

    public Tab(String toolBarName, String url) {
        this.toolBarName = toolBarName;
        this.url = url;
    }

    public String getToolBarName() {
        return toolBarName;
    }

    public String getUrl() {
        return url;
    }

    public void setToolBarName(String toolBarName) {
        this.toolBarName = toolBarName;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTabType() {
        return tabType;
    }

    public void setTabType(int tabType) {
        this.tabType = tabType;
    }

    public void setParentFragment(Fragment parentFragment) {
        this.parentFragment = parentFragment;
    }

    public Fragment getParentFragment() {
        return parentFragment;
    }

}
