package com.example.asus.sarashpazpapillon.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Asus on 9/5/2017.
 */

public class UserList {

    @SerializedName("users")
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
