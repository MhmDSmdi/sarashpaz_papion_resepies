package com.example.asus.sarashpazpapillon.Retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Asus on 9/2/2017.
 */

public class ApiClient{

    public static final String BASE_URL = "http://main.pchef.ir";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static ApiInterface getApiService() {
        return getClient().create(ApiInterface.class);
    }


}

