package com.example.asus.sarashpazpapillon.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.asus.sarashpazpapillon.HelperClasses.SessionManager;
import com.example.asus.sarashpazpapillon.Models.ResepiCard;
import com.example.asus.sarashpazpapillon.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Asus on 8/28/2017.
 */

public class ResepiCardAdapter extends RecyclerView.Adapter<ResepiCardAdapter.MyVeiwHolder> {

    private Context mContext;
    private List<ResepiCard> cardList;
    private SessionManager sessionManager;

    public ResepiCardAdapter(Context mContext, List<ResepiCard> cardList,SessionManager sessionManager ) {
        this.mContext = mContext;
        this.cardList = cardList;
        this.sessionManager = sessionManager;
    }

    @Override
    public MyVeiwHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.resepy_card_layout, parent, false);

        return new MyVeiwHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyVeiwHolder holder, int position) {

        final ResepiCard resepyCard = cardList.get(position);
        holder.title.setText(resepyCard.getTitle());
        holder.likeNumber.setText(resepyCard.getNum_like());
        holder.likeNumber.bringToFront();
        holder.userName.setText(resepyCard.getThisUser().getName());

        Glide.with(mContext)
                .load(resepyCard.getFoodImage())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.guestuser)
                .into(holder.image_thumbnail);

        Glide.with(mContext)
                .load(resepyCard.getThisUser().getProfileImage())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(R.drawable.guestuser)
                .into(holder.icon_thumbnail);

        if (resepyCard.isLiked()) {
            Glide.with(mContext)
                    .load(R.drawable.like)
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.guestuser)
                    .into(holder.like_thumbnail);
        }
        else {
            Glide.with(mContext)
                    .load(R.drawable.like_off)
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.guestuser)
                    .into(holder.like_thumbnail);
        }

        holder.like_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resepyCard.setLiked(!resepyCard.isLiked());
                Log.e("liked", "one item Liked/unLiked");
                sessionManager.setLikeView(resepyCard.getHid(), resepyCard.isLiked());
                if (resepyCard.isLiked()) {
                    Glide.with(mContext)
                            .load(R.drawable.like)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.drawable.guestuser)
                            .into(holder.like_thumbnail);
                }
                else {
                    Glide.with(mContext)
                            .load(R.drawable.like_off)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(R.drawable.guestuser)
                            .into(holder.like_thumbnail);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    class  MyVeiwHolder extends RecyclerView.ViewHolder {
        TextView title, userName, likeNumber;
        ImageView image_thumbnail, like_thumbnail;
        CircleImageView icon_thumbnail;


        public MyVeiwHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            userName = (TextView) itemView.findViewById(R.id.userName);
            likeNumber = (TextView) itemView.findViewById(R.id.count_like);
            image_thumbnail = (ImageView) itemView.findViewById(R.id.main_image);
            icon_thumbnail = (CircleImageView) itemView.findViewById(R.id.profile_image);
            like_thumbnail = (ImageView) itemView.findViewById(R.id.like_image);
        }
    }
}
