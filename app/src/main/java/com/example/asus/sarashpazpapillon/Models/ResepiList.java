package com.example.asus.sarashpazpapillon.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asus on 9/2/2017.
 */

public class ResepiList {

    @SerializedName("recipes")
    private List<ResepiCard> resepiCards;

    public ResepiList() {
        resepiCards = new ArrayList<>();
    }

    public List<ResepiCard> getResepiCards() {
        return resepiCards;
    }

    public void setResepiCards(List<ResepiCard> resepiCards) {
        this.resepiCards = resepiCards;
    }
}
