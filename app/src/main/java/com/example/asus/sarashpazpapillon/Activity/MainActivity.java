package com.example.asus.sarashpazpapillon.Activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Size;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.sarashpazpapillon.Fragments.LastResepi;
import com.example.asus.sarashpazpapillon.Fragments.TopUsersFragment;
import com.example.asus.sarashpazpapillon.Fragments.UserPageResepi;
import com.example.asus.sarashpazpapillon.Models.Tab;
import com.example.asus.sarashpazpapillon.Models.TabList;
import com.example.asus.sarashpazpapillon.R;
import com.example.asus.sarashpazpapillon.Retrofit.ApiClient;
import com.example.asus.sarashpazpapillon.Retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RecyclerView recyclerView;
    private List<Tab> tabs;
    private TextView loadFailed;
    private ImageView reLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabs = new ArrayList<>();
        fetchData();

        loadFailed = (TextView) findViewById(R.id.txtFailorLoad);

        reLoad = (ImageView) findViewById(R.id.reLoadPage);
        reLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchData();
            }
        });

        setupNavigation();
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    private List<Tab> reverseList(List<Tab> temp){
        List<Tab> reversedTabList = new ArrayList<>();
        for(int i = temp.size() - 1 ; i >= 0 ; i--) {
            reversedTabList.add(temp.get(i));
        }
        return reversedTabList;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (Tab a : tabs) {
            Bundle args = new Bundle();
            args.putString("url", getPureURL(a.getUrl()));
            Fragment temp = null;
            switch (a.getTabType()) {
                case 202:
                    temp = new LastResepi();
                    break;

                case 203:
                    temp = new UserPageResepi();
                    break;

                case 204:
                    temp = new TopUsersFragment();
                    break;
            }
            temp.setArguments(args);
            adapter.addFragment(temp, a.getToolBarName());
            a.setParentFragment(temp);
        }
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(viewPager.getAdapter().getCount() - 1, false);

    }

    private void setupNavigation() {
        navigationView = (NavigationView) findViewById(R.id.navigation);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void fetchData() {

        final String tabUrl = "https://api.papillonchef.com/300/op/tabs/2";
        ApiInterface apiInterface =  ApiClient.getApiService();
        Call<TabList> call = apiInterface.getTabList(tabUrl);
        call.enqueue(new Callback<TabList>() {
            @Override
            public void onResponse(Call<TabList> call, Response<TabList> response) {
                loadFailed.setVisibility(View.INVISIBLE);
                reLoad.setVisibility(View.INVISIBLE);
                tabs = reverseList(response.body().getTabs());
                setupViewPager(viewPager);

            }

            @Override
            public void onFailure(Call<TabList> call, Throwable t) {
                loadFailed.setVisibility(View.VISIBLE);
                reLoad.setVisibility(View.VISIBLE);
            }
        });

    }

    private String getPureURL(String url) {
        String temp = "";
        for(int i = 0 ; i < url.length() ; i++){
            if(url.charAt(i) != '@')
                temp += url.charAt(i);
            else
                break;
        }
        return temp;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
