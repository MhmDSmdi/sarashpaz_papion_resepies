package com.example.asus.sarashpazpapillon.Fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.sarashpazpapillon.Adapter.TopUsersRecycler.TopUsersRecyclerAdapter;
import com.example.asus.sarashpazpapillon.Retrofit.ApiClient;
import com.example.asus.sarashpazpapillon.Retrofit.ApiInterface;
import com.example.asus.sarashpazpapillon.HelperClasses.HelperClass;
import com.example.asus.sarashpazpapillon.Models.User;
import com.example.asus.sarashpazpapillon.Models.UserList;
import com.example.asus.sarashpazpapillon.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Asus on 9/5/2017.
 */

public class TopUsersFragment extends Fragment {
    private RecyclerView recyclerView;
    private TopUsersRecyclerAdapter adapter = null;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<User> userList;
    private List<User> userListTemp;
    private FloatingActionButton fab;
    private int indexOffset = 0;
    private String url;
    private View view;
    private TextView txtLoading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString("url");
        userList = new ArrayList<>();
        userListTemp = new ArrayList<>();
        fetchDataByOffset(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.last_resepi, container, false);
        View mview = inflater.inflate(R.layout.activity_main, null);
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        txtLoading = (TextView) getActivity().findViewById(R.id.txtLoading);
        setupScrollListener();
        setupRecycler();
        return view;
    }

    private void setupRecycler() {
        adapter = new TopUsersRecyclerAdapter(view.getContext(), userList);
        mLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void setupScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy>0) {
                    final int visibleThreshold = 1;
                    LinearLayoutManager layoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                    int lastItem  = layoutManager.findLastCompletelyVisibleItemPosition();
                    int currentTotalCount = layoutManager.getItemCount();

                    if(currentTotalCount <= lastItem + visibleThreshold){
                        txtLoading.setVisibility(View.VISIBLE);
                        fetchDataByOffset(indexOffset);
                    }
                    fab.hide();
                }
                else {
                    fab.show();
                }
            }
        });
    }

    private void fetchDataByOffset(int offset) {
        ApiInterface apiInterface = ApiClient.getApiService();
        Call<UserList> call = apiInterface.getUserList(url + offset);
        call.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {
                userListTemp = response.body().getUserList();
                for (User user : userListTemp) {
                    user.setScore(HelperClass.number2farsi(user.getScore()));
                }
                userList.addAll(userListTemp);

                adapter.notifyDataSetChanged();
                indexOffset += 30;
                txtLoading.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {
                Toast.makeText(view.getContext(), "Loading Failed", Toast.LENGTH_LONG).show();
            }
        });
    }
}
