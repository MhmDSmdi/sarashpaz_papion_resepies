package com.example.asus.sarashpazpapillon.HelperClasses;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Asus on 8/30/2017.
 */

public class HelperClass {

    public static String number2farsi(String b)
    {

        Map<String, String> replaceMap = new HashMap<>();
        replaceMap.put("0", "۰");
        replaceMap.put("1", "۱");
        replaceMap.put("2", "۲");
        replaceMap.put("3", "۳");
        replaceMap.put("4", "۴");
        replaceMap.put("5", "۵");
        replaceMap.put("6", "۶");
        replaceMap.put("7", "۷");
        replaceMap.put("8", "۸");
        replaceMap.put("9", "۹");

        for(Map.Entry<String, String> entry : replaceMap.entrySet())
        {
            b = b.replaceAll(entry.getKey(), entry.getValue());
        }

        return b;
    }
}
